<!DOCTYPE html>
<html lang="en">
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detalles del piloto</title>
</head>
<body>


<div class="container">
<table class="table">
  <thead class="thead-dark">
  <tr><p class="h3">Vuelos</p>
      <th scope="col">Numero de vuelo</th>
      <th scope="col">Hora de vuelo</th>
      <th scope="col">Destino</th>
      <th scope="col">Hora de llegada</th>
    </tr>
  </thead>
  <tbody>
    <tr>
    @if(count($pilot->flight)>0)
    @foreach($pilot->flight as $pilot)
      <td>{{$pilot->flight_number}}</td>
      <td>{{$pilot->flight_hour}}</td>
      <td>{{$pilot->destiny}}</td>
      <td>{{$pilot->set_time}}</td>
    </tr>
    @endforeach
    @else
    <h3>El Piloto no a realizado ningun vuelo</h3>
    @endif
  </tbody>
</table>
<hr>

<a href="http://127.0.0.1:8000/base" class="btn btn-primary">Volver al inicio</a>
</div>
</body>
</html>