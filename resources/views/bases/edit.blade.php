<!DOCTYPE html>
<html lang="en">
<head>
<!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Agregar</title>
</head>
<body>
<div class="container">
<form action="/base/{{$base->id}}" method="post">
@method('PUT')
    {{ csrf_field() }}
  <div class="form-group">
    <label for="exampleInputEmail1">Nombre: </label>
    <input type="text" class="form-control" name="name" value="{{$base->name}}">
    <small id="emailHelp" class="form-text text-muted">Ingrese el nombre de la base</small><br>
  </div><br>
  <button type="submit" class="btn btn-primary">Actualizar</button><br><br><a href="http://127.0.0.1:8000/base" class="btn btn-primary">Volver</a>
</form>
</div>
</body>
</html>