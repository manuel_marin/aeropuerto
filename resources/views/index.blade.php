<!DOCTYPE html>
<html lang="en">
<head>
<!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pagina Principal</title>
</head>
<body>
<div class="container">
<table class="table">
  <thead>
    <tr>
      <th scope="col">Nombre</th>
    </tr>
  </thead>
  <tbody>
    <tr>
    @foreach($bases as $base)
      <th scope="row">{{$base->name}}</th>
      <td>
      <a href="http://127.0.0.1:8000/planes" style="border: none; background: white;" name="detalles"><img src="/images/avion.png" style="background: white; height: 20px; width: 20px;" title="Agregar avion"></a>
        <a href="http://127.0.0.1:8000/pilots?idbase={{$base->id}}" style="border: none; background: white;" name="detalles"><img src="/images/piloto.png" style="background: white; height: 20px; width: 20px;" title="Agregar pilotos"></a>
        <a href="http://127.0.0.1:8000/members?baseid={{$base->id}}" style="border: none; background: white;" name="detalles"><img src="/images/comunidad.png" style="background: white; height: 20px; width: 20px;" title="Agregar miembros"></a>
        <a href="http://127.0.0.1:8000/fligths?" style="border: none; background: white;" name="detalles"><img src="/images/vuelo.png" style="background: white; height: 20px; width: 20px;" title="Agregar vuelos"></a>
        <a href="/base/{{$base->id}}" style="border: none; background: white;" name="detalles"><img src="/images/ver.png" style="background: white; height: 20px; width: 20px;" title="Ver Detalles"></a>
        <a href="/base/{{$base->id}}/edit" style="border: none; background: white;" name="detalles"><img src="/images/actualizar.png" style="background: white; height: 20px; width: 20px;" title="Actualizar"></a>
        <form action="{{ route('base.destroy', $base->id)}}" method="POST" class="form form-group">
        @method("DELETE")
        @csrf
        <button type="submit" class="btn btn-primary" style="border:none;background:white;"><img src="/images/eliminar.png" style="background: white; height:20px; width:20px;" title="Eliminar">
        </button>
        </form>
      </td>
    </tr>
    @endforeach
    <a href="http://127.0.0.1:8000/base/create" class="btn btn-danger">Agregar Base</a><br><br>
  </tbody>
</table>
</div>
</body>
</html>
