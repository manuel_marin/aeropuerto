<!DOCTYPE html>
<html lang="en">
<head>
<!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Aviones</title>
</head>
<body><div class="container">
<form action="{{route('plane')}}" method="post">
{{ csrf_field() }}
<div class="form-group">
    <label for="exampleInputEmail1">Codigo del avion </label>
    <input type="text" class="form-control" name="code" placeholder="Ejemplo: 8BF93N">
    <small id="emailHelp" class="form-text text-muted">Ingrese el codigo del avion</small><br>
    <label for="exampleInputEmail1">id </label>
    <input type="text" class="form-control" name="bases_id" >
    <small id="emailHelp" class="form-text text-muted">Ingrese el ID</small>
  </div>
  <button type="submit" class="btn btn-primary">Guardar</button><br><br>
  <a href="http://127.0.0.1:8000/base" class="btn btn-primary">Volver</a>
</form>
</div>
</body>
</html>