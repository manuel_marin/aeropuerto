<!DOCTYPE html>
<html lang="en">
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detalles</title>
</head>
<body>

<div class="container">
<table class="table">
  <thead class="thead-dark">
  <tr><p class="h3">Aviones</p>
      <th scope="col">Codigo</th>
    </tr>
  </thead>
  <tbody>
    <tr>
    @if(count($base->planes)>0)
    @foreach($base->planes as $plane)
      <td>{{$plane->code}}</td>
    </tr>
    @endforeach
    @else
    <h3>La base no tienen aviones</h3>
    @endif
  </tbody>
</table>
<hr>
</div>


<div class="container">
<table class="table">
  <thead class="thead-dark">
  <tr><p class="h3">Pilotos</p>
      <th scope="col">Codigo</th>
    </tr>
  </thead>
  <tbody>
    <tr>
    @if(count($base->pilots)>0)
    @foreach($base->pilots as $pilot)
      <td>{{$pilot->code}} <a href="/piloto/{{$pilot->id}}" class="btn btn-primary">Ver Vuelos del Piloto</a></td>
    </tr>
    @endforeach
    @else
    <h3>La base no tiene Pilotos</h3>
    @endif
  </tbody>
</table>
<hr>
</div>


<div class="container">
<table class="table">
  <thead class="thead-dark">
  <tr><p class="h3">Tripulacion</p>
      <th scope="col">miembros</th>
      <th scope="col">Codigo</th>
    </tr>
  </thead>
  <tbody>
    <tr>
    @if(count($base->members)>0)
    @foreach($base->members as $miembro)
      <td>{{$miembro->name}}</td>
      <td>{{$miembro->code}}</td>
    </tr>
    @endforeach
    @else
    <h3>La base no tiene Tripulacion</h3>
    @endif
  </tbody>
</table>
<a href="http://127.0.0.1:8000/base" class="btn btn-primary">Volver</a>
</div>

</body>
</html>