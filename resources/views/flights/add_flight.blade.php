<!DOCTYPE html>
<html lang="en">
<head>
<!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Aviones</title>
</head>
<body><div class="container">
<form action="{{route('fligth')}}" method="post">
{{ csrf_field() }}
<div class="form-group">
    <label for="exampleInputEmail1">Numero del vuelo </label>
    <input type="text" class="form-control" name="flight_number" placeholder="Ejemplo: 55434">
    <small id="emailHelp" class="form-text text-muted">Ingrese el numero del vuelo</small><br>
    <label for="exampleInputEmail1">Hora del vuelo </label>
    <input type="time" class="form-control" name="flight_hour" placeholder="Ejemplo: 02">
    <small id="emailHelp" class="form-text text-muted">Ingrese la hora del vuelo</small><br>
    <label for="exampleInputEmail1">Destino </label>
    <input type="text" class="form-control" name="destiny" placeholder="Ejemplo: España">
    <small id="emailHelp" class="form-text text-muted">Ingrese el destino</small><br>
    <label for="exampleInputEmail1">Hora de llegada </label>
    <input type="time" class="form-control" name="set_time" placeholder="Ejemplo: 08">
    <small id="emailHelp" class="form-text text-muted">Ingrese la hora de llegada </small><br>
    <label for="exampleInputEmail1">pilots_id </label>
    <input type="text" class="form-control" name="pilots_id" placeholder="Ejemplo: 02">
    <small id="emailHelp" class="form-text text-muted">Ingrese o</small><br>
    <label for="exampleInputEmail1">Planes id </label>
    <input type="text" class="form-control" name="planes_id" placeholder="Ejemplo: 1234">
    <small id="emailHelp" class="form-text text-muted">Ingrese el ID</small>
  </div>
  <button type="submit" class="btn btn-primary">Guardar</button><br><br>
  <a href="http://127.0.0.1:8000/base" class="btn btn-primary">Volver</a>
</form>
</div>
</body>
</html>