<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('base', 'BaseController');

// Route::get('member', 'MemberController@store');
Route::get('members', 'MemberController@create');
Route::post('member/{base_id}', 'MemberController@store')->name('member');

Route::get('planes', 'PlaneController@create');
Route::post('plane', 'PlaneController@store')->name('plane');

Route::get('pilots', 'PilotController@create');
Route::post('pilot/{base_id}', 'PilotController@store')->name('pilot');
Route::get('piloto/{id}', 'PilotController@show');

Route::get('fligths', 'FligthController@create');
Route::post('fligth', 'FligthController@store')->name('fligth');

// Route::post('pilot', ['as' => 'Pilot.store', 'base' => 'PilotController@store']);