<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pilot;

class PilotController extends Controller
{
    public function create(Request $request){
        
        $idbase1 = $request->idbase;
        return view('pilots.add_pilot', compact('idbase1'));
    }

    public function store($idbase,Request $request)
    {

        
        $pilot = new Pilot();

        $pilot->name = $request->name;
        $pilot->flight_time = $request->flight_time;
        $pilot->code = $request->code;
        $pilot->bases_id = $idbase;
        $pilot -> save();

        return redirect()->route('base.index');
    }

    public function show($id)
    {
        $pilot = Pilot::find($id);
        return view('pilots.show', compact('pilot'));
    }
}
