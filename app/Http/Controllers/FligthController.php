<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Flight;

class FligthController extends Controller
{
    public function create(){
        return view('flights.add_flight');
    }

    public function store(Request $request)
    {
        

        $flight = Flight::create($request->all());

        return redirect()->route('base.index');
    }
}
