<?php

namespace App\Http\Controllers;

use App\Models\Crew_member;
use Illuminate\Http\Request;

class MemberController extends Controller
{

    public function create(Request $request){
        $idbase2 = $request->baseid;
        return view('members.add_member', compact('idbase2'));
    }


    public function store($baseid, Request $request)
    {
        $crew = new Crew_member();

        $crew -> name = $request -> name;
        $crew->code = $request->code;
        $crew->bases_id = $baseid;
        $crew -> save();

        return redirect()->route('base.index');
    }

}
