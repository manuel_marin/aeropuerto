<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Plane;
use App\Models\Base;

class PlaneController extends Controller
{
    public function create(){
        return view('planes.plane');
    }

    public function store(Request $request)
    {

        $plane = Plane::create($request->all());
        

        return redirect()->route('base.index');
    }
    
}
