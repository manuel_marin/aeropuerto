<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Base extends Model
{
    use HasFactory;
    protected $table ='bases';

    public $fillable = [
        'name'
    ];

    // public $dates = [
    //     'deleted_at'
    // ];

    public function members(){

        return $this->hasMany('App\Models\Crew_member', 'bases_id');
    }

    public function planes(){
        return $this->hasMany('App\Models\Plane', 'bases_id');
    }

    public function pilots(){
        return $this->hasMany('App\Models\Pilot', 'bases_id');
    }
}
