<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plane extends Model
{
    protected $table ='planes';
    use HasFactory;

    public $fillable = [
        'code',
        'bases_id'
    ];
}
