<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Crew_member extends Model
{
    protected $table ='crew_members';
    use HasFactory;


    public function fligths(){
        return $this->belongsToMany('App\Models\flights_has_crew_members', 'flights_id', 'crew_members_id');
    }
    
}
