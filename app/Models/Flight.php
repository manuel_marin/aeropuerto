<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    protected $table ='flights';
    use HasFactory;

    public function member(){
        return $this->belongsToMany('App\Models\flights_has_crew_members', 'flights_id', 'crew_members_id');
    }

    public $fillable = [
        'flight_number',
        'flight_hour',
        'destiny',
        'set_time',
        'pilots_id',
        'planes_id'
    ];
}
