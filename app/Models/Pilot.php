<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pilot extends Model
{
    protected $table ='pilots';
    use HasFactory;
    
    public function flight(){

        return $this->hasMany('App\Models\Flight', 'pilots_id');
    }
    
}
